package com.paytm.test.index;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONObject;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.paytm.reusable_methods.DBFunctions;
import com.paytm.reusable_methods.Xls_Reader;
import com.paytm.reusable_methods.common_constants;
import com.paytm.reusable_methods.generic_methods;
import com.paytm.test.index.esIndex;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class standaloneProductIndexing 
{
	Connection con=null;
	static ExtentReports extent;
	static ExtentTest log;
	DBFunctions db = new DBFunctions();
	esIndex es = new esIndex();
	generic_methods gm = new generic_methods();
	final String digitalVerticals = "4, 17, 56, 76, 79, 71, 84, 86, 90";
	final String indexTypes[]={"catalog_qc","catalog_v3","catalog_utility","catalog_offline"};
	final static String sheetName = "indexing";
	final static int headerCount=0;
	static int associateComplexid=0;
	static int associateStandaloneid=0;
	static int disablePhysical=0;
	static int disableDigital=0;
	Xls_Reader indexFile;
	Xls_Reader indexResultFile;
	ArrayList<String> testcases = new ArrayList<String>();
	LinkedHashMap<String, Integer> productIds=new LinkedHashMap<String,Integer>();
	complexProductIndexing cp=new complexProductIndexing();
	
	@BeforeTest
	 public void extentReport()
	{	
		 extent = new ExtentReports(common_constants.classPath + "/test/resources/Html_Reports/Indexing/StandaloneIndexingFlow_" + common_constants.currentTimeStamp + ".html", true);
		 extent     .addSystemInfo("Host Name", "Seller-Dev")
		                .addSystemInfo("Environment", "Automation Testing")
		                .addSystemInfo("User Name", "Rashi Agarwal");
		 				extent.loadConfig(new File(System.getProperty("user.dir")+"//extent-config.xml"));
		 extent.endTest(log);
		 extent.flush();
	 }
	
	@BeforeTest
	public void setDiscoverabilityData() throws Exception
	{
		LinkedHashMap<String, String> map=new LinkedHashMap<String,String>();
		con = db.DBConnection(common_constants.serverName);
		
		indexFile = new Xls_Reader(common_constants.classPath+"//test//resources//"+ common_constants.dataFolderName + "//Indexing//standaloneIndexCases.xls");
		indexFile.createExcel(System.getProperty("user.dir")+"/target/Test Results/Indexing/standaloneIndexCases" +"_" + common_constants.currentTimeStamp + ".xls");
		indexResultFile = new Xls_Reader(System.getProperty("user.dir")+"/target/Test Results/Indexing/standaloneIndexCases" +"_" + common_constants.currentTimeStamp + ".xls");
		
		System.out.println("************Excels Created!!!***************");
		log = extent.startTest("Fetch and Update Products!!");
		
		for(int i=2;i<=indexResultFile.getRowCount(sheetName)-3;i++){
			String testCase=indexResultFile.getCellData(sheetName, "Test Cases", i);
			testcases.add(testCase);
		}
		log.log(LogStatus.INFO, "Fetching Product Ids!!!");
		productIds=fetchProducts(testcases);
		if(!productIds.isEmpty())
			log.log(LogStatus.PASS, "Product Ids Fetched!!!");
		else
			log.log(LogStatus.FAIL, "Failed to fetch Products!!!");
		
		for(int i=2;i<=indexResultFile.getRowCount(sheetName)-3;i++)	//for(int i=7;i<=7;i++)
		{
			String testCase=indexResultFile.getCellData(sheetName, "Test Cases", i);
			System.out.println("Test Case:::"+testCase);
			String productId=String.valueOf(productIds.get(testCase));
			if(!productId.isEmpty())
				indexResultFile.setCellData(sheetName, "Product ID", i, productId, headerCount);
			
			if(testCase.toLowerCase().contains("online,b2b and mystore"))
				map.put("onlineb2bmystore", productId);
			else if(testCase.toLowerCase().contains("b2b,mystore"))
				map.put("b2bmystore", productId);
			else if(testCase.toLowerCase().contains("online,b2b"))
				map.put("onlineb2b", productId);
			else if(testCase.toLowerCase().contains("offline,b2b"))
				map.put("offlineb2b", productId);
			else if(testCase.toLowerCase().contains("online,mystore"))
				map.put("onlinemystore", productId);
			else if(testCase.toLowerCase().contains("offline,mystore"))
				map.put("offlinemystore", productId);
			else if(testCase.toLowerCase().contains("b2b"))
				map.put("b2b", productId);
			else if(testCase.toLowerCase().contains("mystore"))
				map.put("mystore", productId);
			else if(testCase.toLowerCase().contains("online")&&!map.containsKey("online"))
				map.put("online", productId);
			else if(testCase.toLowerCase().contains("online"))
				map.put("online", map.get("online")+"|"+productId );
			else if(testCase.toLowerCase().contains("offline")&&!map.containsKey("offline"))
				map.put("offline", productId);
			else if(testCase.toLowerCase().contains("offline"))
				map.put("offline", map.get("offline")+"|"+productId );		
		}
		log = extent.startTest("Update Info of Products!!");
 		for(String key:map.keySet())
		{
			List<Integer> setList=new ArrayList<Integer>();
			String value=map.get(key);
			switch(key)
			{
				case "online":
					setList.add(common_constants.onlineDiscoverability);
					break;
				case "offline":
					setList.add(common_constants.offlineDiscoverability);
					break;
				case "b2b":
					setList.add(common_constants.b2bDiscoverability);
					break;
				case "mystore":
					setList.add(common_constants.mystoreDiscoverability);
					break;
				case "onlineb2b":
					setList.add(common_constants.onlineDiscoverability);
					setList.add(common_constants.b2bDiscoverability);
					break;
				case "onlinemystore":
					setList.add(common_constants.onlineDiscoverability);
					setList.add(common_constants.mystoreDiscoverability);
					break;
				case "offlineb2b":
					setList.add(common_constants.offlineDiscoverability);
					setList.add(common_constants.b2bDiscoverability);
					break;
				case "offlinemystore":
					setList.add(common_constants.offlineDiscoverability);
					setList.add(common_constants.mystoreDiscoverability);
					break;
				case "onlineb2bmystore":
					setList.add(common_constants.onlineDiscoverability);
					setList.add(common_constants.b2bDiscoverability);
					setList.add(common_constants.mystoreDiscoverability);
					break;
				case "b2bmystore":
					setList.add(common_constants.b2bDiscoverability);
					setList.add(common_constants.mystoreDiscoverability);
					break;	
					
			}
			try
			{
				if(!value.contains("|"))
				{
					JSONObject productInfo =gm.setDiscoverabilityInfo(value,setList,con);
					cp.updateInfo(value,productInfo,con);
					System.out.println("After updating product Info:::"+productInfo.toString());
				}
				else
				{
					String[] values=value.split("\\|");
					for(String pidValue:values)
					{
						JSONObject productInfo =gm.setDiscoverabilityInfo(pidValue,setList,con);
						cp.updateInfo(pidValue,productInfo,con);
						System.out.println("After updating product Info:::"+productInfo.toString());
					}
				}
			}
			catch(Exception e)
			{
				System.out.println("Exception caught while updating discoverability of product:::"+value);
				e.printStackTrace();
			}
		}
		log.log(LogStatus.PASS, "Product info has been reset!!!");
		
		extent.endTest(log);
		extent.flush();
	}
	
	@Test
	public void productsIndexed() throws Exception
	{
		LinkedHashMap<String, Object> indexedProductIds = new LinkedHashMap<String,Object>();
		LinkedHashMap<String, Object> expectedindexedProductIds = new LinkedHashMap<String,Object>();
		con = db.DBConnection(common_constants.serverName);
		log = extent.startTest("Update Products!!");
		System.out.println("************************Update Products!!!****************************************");
		
		updateProducts(productIds,indexResultFile);
		Thread.sleep(90000);
		log.log(LogStatus.INFO, "Updation Done!!!");
		
		indexedProductIds=es.hitStandaloneCatalogESIndex(productIds,associateComplexid);
		System.out.println("Actual Indexed Product Ids ::: "+indexedProductIds);
		
		expectedindexedProductIds=expectedCatalogESIndex(productIds,indexResultFile);
		System.out.println("Expected Indexed Product Ids ::: "+expectedindexedProductIds);
		
		validateData(indexedProductIds,expectedindexedProductIds,indexResultFile);
		
		extent.endTest(log);
		extent.flush();
			
	}
	
	@SuppressWarnings("unchecked")
	public static void validateData(LinkedHashMap<String, Object> actualIndexedData,LinkedHashMap<String, Object> expectedIndexedData,Xls_Reader indexResult)
	{
		
		String status="";
		for(int rowNum=2;rowNum<=indexResult.getRowCount(sheetName)-3;rowNum++)
		{
			String testCase=indexResult.getCellData(sheetName, "Test Cases", rowNum);
			log = extent.startTest("TestCase::: "+testCase);
			System.out.println("*************************Validating for testCase::: "+testCase+"********************************");
			log.log(LogStatus.INFO,"Validate Data!!" );
			
			LinkedHashMap<String, Boolean> actualProductindexesValues = new LinkedHashMap<String, Boolean>();
			LinkedHashMap<String, Boolean> expectedProductindexesValues = new LinkedHashMap<String, Boolean>();
			LinkedHashMap<String, String> failedIndex = new LinkedHashMap<String, String>();
			if(actualIndexedData.containsKey(testCase) && expectedIndexedData.containsKey(testCase))
			{
				actualProductindexesValues=(LinkedHashMap<String, Boolean>) actualIndexedData.get(testCase);
				expectedProductindexesValues=(LinkedHashMap<String, Boolean>) expectedIndexedData.get(testCase);
				System.out.println("Actual::"+actualProductindexesValues);
				System.out.println("Expected:::"+expectedProductindexesValues);
				failedIndex.clear();
				ArrayList<String> tempResult=new ArrayList<>();
				for(String key:actualProductindexesValues.keySet())
				{
					boolean actual=actualProductindexesValues.get(key);
					boolean expected=expectedProductindexesValues.get(key);
					
					if(actual==expected)
						tempResult.add("Pass");
					else
					{
						tempResult.add("Fail");
						failedIndex.put(key,"Actual Data::: "+"->>"+actual+" :::: Expected Data:::"+expected);
					}
				}
				if(!tempResult.contains("Fail"))
					status="Pass";
				else
					status="Fail";
			}
			else
				status="Fail || Either actual map or expected map does not contain keys";
			
			if(!status.contains("Fail"))
			{
				log.log(LogStatus.PASS, "Product got indexed on expected indexes");
				log.log(LogStatus.INFO, "Actual Data::: "+actualProductindexesValues);
				log.log(LogStatus.INFO, "Expected Data::: "+expectedProductindexesValues);
			}
			else
			{
				log.log(LogStatus.FAIL, "Product does not got indexed on expected indexes");
				log.log(LogStatus.INFO, "Failed Indexes!!!!");
				log.log(LogStatus.INFO, failedIndex.toString());
				
			}
			
			indexResult.setCellData(sheetName, "Actual Result", rowNum, actualProductindexesValues.toString(), headerCount);
			indexResult.setCellData(sheetName, "Expected Result", rowNum, expectedProductindexesValues.toString(), headerCount);
			indexResult.setCellData(sheetName, "Result", rowNum, status, headerCount);
		}	
	}
	
	public LinkedHashMap<String, Object> expectedCatalogESIndex(LinkedHashMap<String, Integer> productids,Xls_Reader indexResult)
	{
		LinkedHashMap<String, Object> expectedIndexed = new LinkedHashMap<String,Object>();
		for(int rowNum=2;rowNum<=indexResult.getRowCount(sheetName)-3;rowNum++)
		{
			LinkedHashMap<String, Boolean> indexValues = new LinkedHashMap<String, Boolean>();
			String testCase=indexResult.getCellData(sheetName, "Test Cases", rowNum);
			
			String productid=indexResult.getCellData(sheetName, "Product ID", rowNum);
			String catalog_qc_value=indexResult.getCellData(sheetName, "catalog_qc", rowNum);
			String catalog_utility_value=indexResult.getCellData(sheetName, "catalog_utility", rowNum);
			String catalog_v3_value=indexResult.getCellData(sheetName, "catalog_v3", rowNum);
			String catalog_offline_value=indexResult.getCellData(sheetName, "catalog_offline", rowNum);
			String catalog_b2b_value=indexResult.getCellData(sheetName, "catalog_b2b_v1", rowNum);
			String catalog_mystore_value=indexResult.getCellData(sheetName, "catalog_mystore_v1", rowNum);
			
			String search_index_value=indexResult.getCellData(sheetName, "search", rowNum);
			String search_index_offline_value=indexResult.getCellData(sheetName, "offline_products(search)", rowNum);
			String search_index_b2b_value=indexResult.getCellData(sheetName, "search_b2b", rowNum);
			String search_index_mystore_value=indexResult.getCellData(sheetName, "search_mystore", rowNum);
			String search_by_complex_value=indexResult.getCellData(sheetName, "search by complex", rowNum);
			
			indexValues.put(productid+"_catalog_qc",Boolean.valueOf(catalog_qc_value));
			indexValues.put(productid+"_catalog_v3",Boolean.valueOf(catalog_v3_value));
			indexValues.put(productid+"_catalog_utility_v3",Boolean.valueOf(catalog_utility_value));
			indexValues.put(productid+"_catalog_offline",Boolean.valueOf(catalog_offline_value));
			indexValues.put(productid+"_catalog_b2b_v1",Boolean.valueOf(catalog_b2b_value));
			indexValues.put(productid+"_catalog_mystore_v1",Boolean.valueOf(catalog_mystore_value));
			indexValues.put(productid+"_search_v4",Boolean.valueOf(search_index_value));
			indexValues.put(productid+"_offline_products",Boolean.valueOf(search_index_offline_value));
			if(testCase.toLowerCase().contains("associate"))
			{
				String value=search_by_complex_value.substring(0,search_by_complex_value.indexOf("|"));
				indexValues.put(associateComplexid+"_search_by_complex",Boolean.valueOf(value));
			}
			indexValues.put(productid+"_b2b_alias",Boolean.valueOf(search_index_b2b_value));
			indexValues.put(productid+"_mystore_alias",Boolean.valueOf(search_index_mystore_value));
			
			expectedIndexed.put(testCase, indexValues);
		}
		
		return expectedIndexed;
		
	}
	
	public LinkedHashMap<String, Integer> fetchProducts(ArrayList<String> testCases) throws Exception
	{
		ArrayList<ArrayList<String>> queryResult = new ArrayList<ArrayList<String>>();
		LinkedHashMap<String, Integer> productIds=new LinkedHashMap<String,Integer>();
		for(String testcase:testCases)
		{
			String query="";
			System.out.println("Working for test case ::: "+testcase);
			switch(testcase.toLowerCase())
			{
				case "qc  type product":
					query="select cp.id from catalog_product cp join mktplace.inventory iv on cp.id=iv.product_id where cp.custom_int_5!=6 and cp.custom_int_5!=0 and cp.vertical_id=2 and cp.status=1 and parent_id is null and product_type=1 and iv.qty!=0 order by cp.id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int qcProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,qcProducts);
						System.out.println("QC Product ID:"+qcProducts);
					}
					break;
				case "digital type product":
					query="select cp.id from catalog_product cp join mktplace.inventory iv on cp.id=iv.product_id where custom_int_5=6 and cp.status=1 and vertical_id in("+digitalVerticals+") and parent_id is null and product_type=1 and iv.qty!=0 order by id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int digitalProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,digitalProducts);
						System.out.println("Digital Product ID:"+digitalProducts);
					}
					break;
				case "physical standalone online product":
						productIds.put(testcase, 1200167724);
						System.out.println("Physical Online Product ID: 1200167724");
					break;
				case "physical standalone offline product":
					productIds.put(testcase, 1200171205);
					System.out.println("Physical Offline Product ID: 1200171205");
					break;	
				case "update qc product to live online":
					String qcProductId=String.valueOf(productIds.get("QC  Type Product"));
					query="select cp.id from catalog_product cp join mktplace.inventory iv on cp.id=iv.product_id where cp.custom_int_5!=6 and cp.vertical_id=2 and cp.status=1 and parent_id is null and product_type=1 and cp.id not in("+qcProductId+") and iv.qty!=0 order by cp.id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int qcProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,qcProducts);
						System.out.println("Update QC to LIVE:"+qcProducts);
					}
					//productIds.put(testcase, productIds.get("QC Online Type Product"));
					break;
				case "disable physical standalone online product":
					query="select cp.id from catalog_product cp join mktplace.inventory iv  on cp.id=iv.product_id where custom_int_5=6 and vertical_id=2 and product_type=1 and parent_id is null and cp.status=1 and iv.qty!=0 order by id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int physicalProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,physicalProducts);
						System.out.println("Disable Physical:"+physicalProducts);
						disablePhysical=physicalProducts;
					}
					
					break;
				case "disable digital product":
					String digitalProductId=String.valueOf(productIds.get("Digital type Product"));
					query="select cp.id from catalog_product cp join mktplace.inventory iv  on cp.id=iv.product_id where custom_int_5=6 and vertical_id in("+digitalVerticals+") and cp.id not in ("+digitalProductId+") and cp.status=1 and parent_id is null and product_type=1 and iv.qty!=0 order by cp.id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int digitalProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,digitalProducts);
						System.out.println("Disable Digital:"+digitalProductId);
						disableDigital=digitalProducts;
					}
					break;
				case "physical standlaone online product oos":
					query="select cp.id from catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id where cp.product_type=1 and cp.parent_id is null and cp.status=1 and cp.vertical_id=2 and inv.qty=0 and inv.updated_at between ((CURRENT_DATE()) - INTERVAL 15 DAY) AND (CURRENT_DATE()) order by cp.id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int physicalOOSProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,physicalOOSProducts);
						System.out.println("Disable Physical OOS:"+physicalOOSProducts);
					}
					break;	
				case "associate physical standalone online to complex product":
					productIds.put(testcase,1200171766);
					System.out.println("Associate:1200171766");
					/*query=" select cp.id from catalog_product cp join mktplace.inventory iv on cp.id=iv.product_id where custom_int_5=6 and cp.status=1 and parent_id is null and product_type=1 and iv.qty!=0 and vertical_id=2 and category_id=8544 order by cp.id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int physicalOOSProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,physicalOOSProducts);
					}*/
					break;	
				case "b2b product":
					productIds.put(testcase,1200172610);
					System.out.println("B2B:1200172610");
					break;
				case "mystore product":
					productIds.put(testcase,1200172612);
					System.out.println("MyStore:1200172612");
					break;
				case "b2b,mystore product":
					productIds.put(testcase,1200172611);
					System.out.println("B2B,Mystore:1200172611");
					break;
				case "physical online,b2b product":
					productIds.put(testcase,1200172613);
					System.out.println("Physical online, B2B Product:1200172613");
					break;
				case "physical offline,mystore product":
					productIds.put(testcase,1200172614);
					System.out.println("Physical Offline,Mystore Product:1200172614");
					break;
				case "physical online,b2b and mystore product":
					productIds.put(testcase,1200172729);
					System.out.println("Physical,B2B,Mystore:1200172729");
					break;
				case "physical offline,b2b product":
					productIds.put(testcase,1200172732);
					System.out.println("Physical Offline B2B:1200172732");
					break;
				case "physical online,mystore product":
					productIds.put(testcase,1200172736);
					System.out.println("Physical Online,Mystore Product:1200172736");
					break;
					
					
				case "change discoverability of physical standalone online product":
					query="select id from catalog_product where parent_id is null and product_type=1 and custom_int_5=6 and status=1 and id not in("+1200170801+") order by id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int changediscoverabilityProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,changediscoverabilityProducts);
					}
					break;
					
			}
		}
		
		return productIds;
	}
	
	public void updateProducts(LinkedHashMap<String, Integer> productids,Xls_Reader indexResult) throws Exception
	{
		for(String testCase:productids.keySet())
		{
			String query="";
			int update=0;
			String productId=String.valueOf(productids.get(testCase));
			switch(testCase.toLowerCase())
			{
				case "qc type product":
				case "digital type product":
				case "physical standalone online product":
				case "physical standalone offline product":
					query="update catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id set cp.price=cp.price+1,inv.updated_at=now() where cp.id="+productId;
					update=db.insertUpdateRecordWithConnectObj(con, query);
					break;
				case "b2b product":
				case "mystore product":
				case "b2b,mystore product":
				case "physical online,b2b product":
				case "physical offline,b2b product":
				case "physical online,mystore product":
				case "physical offline,mystore product":
				case "physical online,b2b and mystore product":
					query="update catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id set cp.price=cp.price+1,cp.status=1,cp.custom_int_5=6,inv.qty=1000,inv.updated_at=now() where cp.id="+productId;
					update=db.insertUpdateRecordWithConnectObj(con, query);
					break;
				case "update qc product to live online":
					query="update catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id set cp.price=cp.price+1,cp.custom_int_5=6,inv.updated_at=now() where cp.id="+productId;
					update=db.insertUpdateRecordWithConnectObj(con, query);
					break;
				case "disable physical standalone online product":
				case "disable digital product":
					query="update catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id set cp.status=0,cp.price=cp.price+1,inv.updated_at=now() where cp.id="+productId;
					update=db.insertUpdateRecordWithConnectObj(con, query);
					break;
				case "physical standlaone online product oos":
					query="update catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id set cp.price=cp.price+1,cp.custom_int_5=6,inv.qty=0 where cp.id="+productId;
					update=db.insertUpdateRecordWithConnectObj(con, query);
					break;
				case "associate physical standalone online to complex product":
					String value=indexResult.getCellData(sheetName, "search by complex", 10);
					String complexProductid=value.substring(value.indexOf("|")+1, value.length());
					associateComplexid=Integer.valueOf(complexProductid);
					associateStandaloneid=Integer.valueOf(productId);
					query="update catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id set cp.price=cp.price+1,parent_id="+associateComplexid+",inv.updated_at=now(),visibility=3 where cp.id="+productId;
					update=db.insertUpdateRecordWithConnectObj(con, query);
					break;
			}
			
		}
	}
	
	@AfterTest
	public void revertData() throws Exception
	{
		con = db.DBConnection(common_constants.serverName);
		String query="update catalog_product set parent_id=null,visibility=4 where id="+associateStandaloneid;
		int update=db.insertUpdateRecordWithConnectObj(con, query);
		System.out.println("Updated data reverted ::: "+update);
		
		query="update catalog_product set status=1 where id in("+disablePhysical+","+disableDigital+")";
		update=db.insertUpdateRecordWithConnectObj(con, query);
		System.out.println("Updated data reverted ::: "+update);
		
		
		
	}
}
