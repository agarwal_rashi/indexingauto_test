package com.paytm.test.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;

import com.paytm.reusable_methods.DBFunctions;
import com.paytm.reusable_methods.common_constants;
import com.paytm.reusable_methods.generic_methods;
import com.paytm.test.index.esIndex;

public class productIndexing 
{

	Connection con=null;
	FileWriter fileWriter = null;
	esIndex es = new esIndex();
	DBFunctions db = new DBFunctions();
	generic_methods gm = new generic_methods();
	final String sourceFilePath = common_constants.classPath+"/test/resources/Test Data/Indexing/";
	final String destFilePath = common_constants.classPath+"/test/resources/Test Results/Indexing/";
	final String fileName="indexing.csv";
	File source = new File(sourceFilePath+fileName);
    File dest = new File(destFilePath+fileName+common_constants.currentTimeStamp);
	final String COMMA_DELIMITER = ",";
	final int index = 0;
	final String digitalVerticals = "4, 17, 56, 76, 79, 71, 84, 86, 90";
	//String testCase="";
	
	@Test
	public void productsIndexed() throws Exception
	{
		ArrayList<String> testcases = new ArrayList<String>();
		LinkedHashMap<String, Integer> productIds=new LinkedHashMap<String,Integer>();
		con = db.DBConnection(common_constants.serverName);
		testcases=readCSVFile(sourceFilePath,fileName);
		for(int i=0;i<testcases.size();i++)	
		{
			System.out.println("Test Case :::"+testcases.get(i));
		}
		
		productIds=fetchProducts(testcases);
		System.out.println("Product ids fetched:::" +productIds);
		
		gm.copyFileUsingApacheCommonsIO(source,dest);
		//dest=writeCSV(productIds,"Product ID",dest);
		BufferedReader fileReader = null;
		try
		{
			String line = "";
			fileWriter=new FileWriter(dest);
			fileReader = new BufferedReader(new FileReader(dest));
			fileReader.readLine();
			while ((line = fileReader.readLine()) != null) 
			{
				String[] values = line.split(COMMA_DELIMITER);
				if(values[1].equals("Product ID")) {
					fileWriter.append(productIds.get("QC Online Type Product").toString());
		        }
			}
		}
		catch (Exception e)
		{
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}
		finally 
		{
			try
			{
		      fileWriter.flush();
		      fileWriter.close();
			}
			catch (IOException e) 
			{
		        System.out.println("Error while flushing/closing fileWriter !!!");
		        e.printStackTrace();
		    }
		}
		
		
	}
	
	
	public LinkedHashMap<String, Integer> fetchProducts(ArrayList<String> testCases) throws Exception
	{
		ArrayList<ArrayList<String>> queryResult = new ArrayList<ArrayList<String>>();
		LinkedHashMap<String, Integer> productIds=new LinkedHashMap<String,Integer>();
		for(String testcase:testCases)
		{
			String query="";
			switch(testcase.toLowerCase())
			{
				case "qc online type product":
					query="select id from catalog_product where custom_int_5!=6 and custom_int_5!=0 and vertical_id=2 order by id desc limit 1";
					//result=db.SelectQuery(1, query, con);
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int qcProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,qcProducts);
					}
					break;
				case "digital type product":
					query="select id from catalog_product where custom_int_5=6 and vertical_id in("+digitalVerticals+") order by id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int digitalProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,digitalProducts);
					}
					break;
				case "physical standalone online product":
						productIds.put(testcase, 1200170801);;
					break;
				case "physical standalone offline product":
					productIds.put(testcase, 1200171205);
					break;	
				case "update qc product to live":
					productIds.put(testcase, productIds.get("QC Online Type Product"));
					break;
				case "disable physical standalone product":
					query="select id from catalog_product where custom_int_5=6 and vertical_id=2 and product_type=1 and parent_id is null order by id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int physicalProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,physicalProducts);
					}
					break;
				case "disable digital product":
					productIds.put(testcase, productIds.get("Digital type Product"));
					break;
				case "physical standlaone product oos":
					query="select cp.id from catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id where cp.product_type=1 and cp.parent_id is null and cp.vertical_id=2 and inv.qty=0 and inv.updated_at between ((CURRENT_DATE()) - INTERVAL 15 DAY) AND (CURRENT_DATE()) order by cp.id desc limit 1";
					queryResult=db.getAllRecordsUsingExistingConnection(con, query);
					for (int i = 0; i < queryResult.size(); i++)
					{
						int physicalOOSProducts=Integer.valueOf(queryResult.get(i).toString().replaceAll("\\[", "").replaceAll("\\]", ""));
						productIds.put(testcase,physicalOOSProducts);
					}
					break;	
			}
		}
		
		return productIds;
	}
	
	public File writeCSV(LinkedHashMap<String, Integer> productids,String columnHeaderName,File dest)
	{
		BufferedReader fileReader = null;
		try
		{
			String line = "";
			fileWriter=new FileWriter(dest);
			fileReader = new BufferedReader(new FileReader(dest));
			fileReader.readLine();
			while ((line = fileReader.readLine()) != null) 
			{
				String[] values = line.split(COMMA_DELIMITER);
				if(values[1].equals(columnHeaderName)) {
					fileWriter.append(productids.get("QC Online Type Product").toString());
		        }
			}
		}
		catch (Exception e)
		{
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}
		finally 
		{
			try
			{
		      fileWriter.flush();
		      fileWriter.close();
			}
			catch (IOException e) 
			{
		        System.out.println("Error while flushing/closing fileWriter !!!");
		        e.printStackTrace();
		    }
		}
		return dest;
	}
	
	public ArrayList<String> readCSVFile(String filePath,String fileName) 
	{
		ArrayList<String> testcases = new ArrayList<String>();
		BufferedReader fileReader = null;
		try 
		{
			String line = "";
			fileReader = new BufferedReader(new FileReader(filePath+"/"+fileName));
			fileReader.readLine();
			while ((line = fileReader.readLine()) != null) 
			{
				String[] cases = line.split(COMMA_DELIMITER);
				testcases.add(cases[index]);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("Error in CsvFileReader !!!");
			e.printStackTrace();
		} finally 
		{
			try {
				fileReader.close();
			} catch (IOException e) {
				System.out.println("Error while closing fileReader !!!");
				e.printStackTrace();
			}
		}
		return testcases;
	}
}
