package com.paytm.test.index;

import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONObject;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.paytm.reusable_methods.DBFunctions;
import com.paytm.test.index.esIndex;
import com.paytm.reusable_methods.Xls_Reader;
import com.paytm.reusable_methods.common_constants;
import com.paytm.reusable_methods.generic_methods;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class complexProductIndexing 
{
	
	static ExtentReports extent;
	static ExtentTest log;
	DBFunctions db = new DBFunctions();
	esIndex es = new esIndex();
	generic_methods gm = new generic_methods();
	final String indexTypes[]={"catalog_qc","catalog_v3","catalog_offline","catalog_utility_v3","catalog_b2b_v1","catalog_mystore_v1"};
	final String searchindexTypes[]={"search_v4","offline_products","b2b_alias","mystore_alias"};
	final static String sheetName = "complexIndexing";
	final static int headerCount=0;
	static int associateComplexid=0;
	String disableStandaloneUtility="";
	String dissociateOnlineProduct="";
	Xls_Reader indexFile ;
	Xls_Reader indexResultFile;
	ArrayList<String> testcases = new ArrayList<String>();
	LinkedHashMap<String, Integer> productIds=new LinkedHashMap<String,Integer>();
	Connection con=null;
	
	@BeforeTest
	 public void extentReport()
	{	
		 extent = new ExtentReports(common_constants.classPath + "/test/resources/Html_Reports/Indexing/ComplexIndexingFlow_" + common_constants.currentTimeStamp + ".html", true);
		 extent     .addSystemInfo("Host Name", "Seller-Dev")
		                .addSystemInfo("Environment", "Automation Testing")
		                .addSystemInfo("User Name", "Rashi Agarwal");
		 				extent.loadConfig(new File(System.getProperty("user.dir")+"//extent-config.xml"));
		 extent.endTest(log);
		 extent.flush();
	 }
	
	@BeforeTest
	public void setDiscoverabilityData() throws Exception
	{
		indexFile = new Xls_Reader(common_constants.classPath+"//test//resources//"+ common_constants.dataFolderName + "//Indexing//complexIndexCases.xls");
		indexFile.createExcel(System.getProperty("user.dir")+"/target/Test Results/Indexing/complexIndexCases" +"_" + common_constants.currentTimeStamp + ".xls");
		indexResultFile = new Xls_Reader(System.getProperty("user.dir")+"/target/Test Results/Indexing/complexIndexCases" +"_" + common_constants.currentTimeStamp + ".xls");
		
		LinkedHashMap<Integer, Object> complexVariantsMap=new LinkedHashMap<Integer,Object>();
		con = db.DBConnection(common_constants.serverName);
		System.out.println("********Excels Created!!!*********");
		log = extent.startTest("Set Product INFO!!");
		for(int i=2;i<=indexResultFile.getRowCount(sheetName)-1;i++)
		//for(int i=7;i<=7;i++)
		{
			LinkedHashMap<String, String> variantsMap=new LinkedHashMap<String,String>();
			String testCase=indexResultFile.getCellData(sheetName, "Test Cases", i);
			int productId=Integer.valueOf(indexResultFile.getCellData(sheetName, "Complex Product ID", i));
			String onlineproductId=indexResultFile.getCellData(sheetName, "catalog_v3", i);
			String offlineproductId=indexResultFile.getCellData(sheetName, "catalog_offline", i);
			String b2bproductId=indexResultFile.getCellData(sheetName, "catalog_b2b_v1", i);
			String mystoreproductId=indexResultFile.getCellData(sheetName, "catalog_mystore_v1", i);
			
			variantsMap.put("online", onlineproductId);
			variantsMap.put("offline", offlineproductId);
			variantsMap.put("b2b", b2bproductId);
			variantsMap.put("mystore", mystoreproductId);
			
			testcases.add(testCase);
			productIds.put(testCase,productId);
			complexVariantsMap.put(productId, variantsMap);
			
			//Handling for test case when all possible combinations are present on complex product id
			if(testCase.contains("all"))
			{
				int complexid=productIds.get(testCase);
				LinkedHashMap<String, String> fetchMap=new LinkedHashMap<String,String>();
				fetchMap=(LinkedHashMap<String, String>) complexVariantsMap.get(complexid);
				String onlinepids=fetchMap.get("online");
				String offlinepids=fetchMap.get("offline");
				String b2bpids=fetchMap.get("b2b");
				String mystorepids=fetchMap.get("mystore");
				String[] online=onlinepids.split("\\|");
				String[] offline=offlinepids.split("\\|");
				String[] b2b=b2bpids.split("\\|");
				for(String onlineValue:online)
				{
					if(b2bpids.contains(onlineValue)&&variantsMap.containsKey("onlineb2b")&!mystorepids.contains(onlineValue))
						variantsMap.put("onlineb2b", variantsMap.get("onlineb2b")+"|"+onlineValue);
					else if(b2bpids.contains(onlineValue)&&!mystorepids.contains(onlineValue))
						variantsMap.put("onlineb2b", onlineValue);
					else if(mystorepids.contains(onlineValue)&&variantsMap.containsKey("onlinemystore")&!b2bpids.contains(onlineValue))
						variantsMap.put("onlinemystore", variantsMap.get("onlinemystore")+"|"+onlineValue);
					else if(mystorepids.contains(onlineValue)&!b2bpids.contains(onlineValue))
						variantsMap.put("onlinemystore", onlineValue);
					else if(b2bpids.contains(onlineValue)&&mystorepids.contains(onlineValue))
						variantsMap.put("onlineb2bmystore", onlineValue);
				}
				
				for(String offlineValue:offline)
				{
					if(b2bpids.contains(offlineValue)&&variantsMap.containsKey("offlineb2b")&&!mystorepids.contains(offlineValue))
						variantsMap.put("offlineb2b", variantsMap.get("offlineb2b")+"|"+offlineValue);
					else if(b2bpids.contains(offlineValue)&&!mystorepids.contains(offlineValue))
						variantsMap.put("offlineb2b", offlineValue);
					else if(mystorepids.contains(offlineValue)&&variantsMap.containsKey("offlinemystore")&&!b2bpids.contains(offlineValue))
						variantsMap.put("offlinemystore", variantsMap.get("offlinemystore")+"|"+offlineValue);
					else if(mystorepids.contains(offlineValue)&&!b2bpids.contains(offlineValue))
						variantsMap.put("offlinemystore", offlineValue);
				}
				for(String b2bValue:b2b)
				{
					if(mystorepids.contains(b2bValue)&&!onlinepids.contains(b2bValue)&&!offlinepids.contains(b2bValue))
						variantsMap.put("b2bmystore", b2bValue);
				}
				
				variantsMap.remove("online");
				variantsMap.remove("offline");
				variantsMap.remove("b2b");
				variantsMap.remove("mystore");
				//complexVariantsMap.put(complexid, variantsMap);
			}
			
			
		}
		System.out.println("Product id map with test cases:::"+productIds);
		System.out.println("Complex Variants Map:::"+complexVariantsMap);
		
		for(Integer complexid:complexVariantsMap.keySet())
		{
			LinkedHashMap<String, String> fetchMap=new LinkedHashMap<String,String>();
			fetchMap=(LinkedHashMap<String, String>) complexVariantsMap.get(complexid);
			for(String key:fetchMap.keySet())
			{
				List<Integer> setList=new ArrayList<Integer>();
				String value=fetchMap.get(key);
				switch(key)
				{
					case "online":
						setList.add(common_constants.onlineDiscoverability);
						break;
					case "offline":
						setList.add(common_constants.offlineDiscoverability);
						break;
					case "b2b":
						setList.add(common_constants.b2bDiscoverability);
						break;
					case "mystore":
						setList.add(common_constants.mystoreDiscoverability);
						break;
					case "onlineb2b":
						setList.add(common_constants.onlineDiscoverability);
						setList.add(common_constants.b2bDiscoverability);
						break;
					case "onlinemystore":
						setList.add(common_constants.onlineDiscoverability);
						setList.add(common_constants.mystoreDiscoverability);
						break;
					case "offlineb2b":
						setList.add(common_constants.offlineDiscoverability);
						setList.add(common_constants.b2bDiscoverability);
						break;
					case "offlinemystore":
						setList.add(common_constants.offlineDiscoverability);
						setList.add(common_constants.mystoreDiscoverability);
						break;
					case "onlineb2bmystore":
						setList.add(common_constants.onlineDiscoverability);
						setList.add(common_constants.b2bDiscoverability);
						setList.add(common_constants.mystoreDiscoverability);
						break;
					case "b2bmystore":
						setList.add(common_constants.b2bDiscoverability);
						setList.add(common_constants.mystoreDiscoverability);
						break;	
						
				}
				
				if(!value.contentEquals("FALSE")&&!value.contains("|"))
				{
					JSONObject productInfo =gm.setDiscoverabilityInfo(value,setList,con);
					updateInfo(value,productInfo,con);
				}
				else if(value.contains("|"))
				{
					String[] values=value.split("\\|");
					for(String pidValue:values)
					{
						JSONObject productInfo =gm.setDiscoverabilityInfo(pidValue,setList,con);
						updateInfo(pidValue,productInfo,con);
					}
				}		
			}
		}
		
		log.log(LogStatus.PASS, "Product info has been reset!!!");
		
		extent.endTest(log);
		extent.flush();
	}
	
	@Test
	public void productsIndexed() throws Exception
	{
		LinkedHashMap<String, Object> indexedProductIds = new LinkedHashMap<String,Object>();
		LinkedHashMap<String, Object> expectedindexedProductIds = new LinkedHashMap<String,Object>();
		
		con = db.DBConnection(common_constants.serverName);
		System.out.println("Product id map with test cases:::"+productIds);
		log = extent.startTest("Update Products!!");
		System.out.println("***********Update Products!!!*************");
		updateProducts(productIds,indexResultFile);
		Thread.sleep(90000);
		log.log(LogStatus.INFO, "Updation Done!!!");
			 
		indexedProductIds=es.hitComplexCatalogESIndex(productIds,indexTypes,searchindexTypes,dissociateOnlineProduct);
		System.out.println("Actual Indexed Product Ids ::: "+indexedProductIds);
		
		expectedindexedProductIds=expectedComplexCatalogESIndex(productIds,indexResultFile);
		System.out.println("Expected Indexed Product Ids ::: "+expectedindexedProductIds);
		
		validateData(indexedProductIds,expectedindexedProductIds,indexResultFile);
		
		extent.endTest(log);
		extent.flush();
	}
	
	public void updateProducts(LinkedHashMap<String, Integer> productids,Xls_Reader indexResult) throws Exception
	{
		for(int rowNum=2;rowNum<=indexResult.getRowCount(sheetName)-1;rowNum++)
		//for(int rowNum=7;rowNum<=7;rowNum++)
		{
			String query="";
			int update=0;
			String testCase=indexResult.getCellData(sheetName, "Test Cases", rowNum);
			String catalog_qc_pid=indexResult.getCellData(sheetName, "catalog_qc", rowNum);
			String catalog_v3_pid=indexResult.getCellData(sheetName, "catalog_v3", rowNum);
			String catalog_utility_pid=indexResult.getCellData(sheetName, "catalog_utility", rowNum);
			String catalog_b2b_pid=indexResult.getCellData(sheetName, "catalog_b2b_v1", rowNum);
			String catalog_mystore_pid=indexResult.getCellData(sheetName, "catalog_mystore_v1", rowNum);
			if(testCase.toLowerCase().contains("online")&& !testCase.toLowerCase().contains("dissociate"))
			{
				query="update catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id set cp.price=cp.price+1,cp.status=1,cp.custom_int_5=5,inv.updated_at=now() where cp.id="+catalog_qc_pid;
				update=db.insertUpdateRecordWithConnectObj(con, query);
			}
			else if(testCase.toLowerCase().contains("disabled digital"))
			{
				disableStandaloneUtility=catalog_utility_pid.substring(0,catalog_utility_pid.indexOf("|"));
				String activeVaraiantUtility=catalog_utility_pid.substring(catalog_utility_pid.indexOf("|")+1, catalog_utility_pid.length());
				query="update catalog_product set price=price+1,status=0,custom_int_5=6 where id="+disableStandaloneUtility;
				update=db.insertUpdateRecordWithConnectObj(con, query);
				
				query="update catalog_product set price=price+1,status=1,custom_int_5=6 where id="+activeVaraiantUtility;
				update=db.insertUpdateRecordWithConnectObj(con, query);
				
				indexResult.setCellData(sheetName, "catalog_utility", rowNum, activeVaraiantUtility, headerCount);
			}
			else if(testCase.toLowerCase().contains("one digital"))
			{
				String updateStandaloneUtility=catalog_utility_pid.substring(catalog_utility_pid.indexOf("|")+1, catalog_utility_pid.length());
				query="update catalog_product set price=price+1,status=1,custom_int_5=6 where id="+updateStandaloneUtility;
				update=db.insertUpdateRecordWithConnectObj(con, query);
			}
			else if(testCase.toLowerCase().contains("dissociate online"))
			{
				dissociateOnlineProduct=catalog_v3_pid.substring(0,catalog_v3_pid.indexOf("|"));
				String associatedOnlineProduct=catalog_v3_pid.substring(catalog_v3_pid.indexOf("|")+1, catalog_v3_pid.length());
				query="update catalog_product set price=price+1,parent_id=null,visibility=4,status=1,custom_int_5=6 where id="+dissociateOnlineProduct;
				update=db.insertUpdateRecordWithConnectObj(con, query);
				
				query="update catalog_product set price=price+1,status=1,custom_int_5=6 where id="+associatedOnlineProduct;
				update=db.insertUpdateRecordWithConnectObj(con, query);
				
				indexResult.setCellData(sheetName, "catalog_v3", rowNum, associatedOnlineProduct, headerCount);
				indexResult.setCellData(sheetName, "search_v4", rowNum, associatedOnlineProduct, headerCount);
			}
			else if(testCase.toLowerCase().contains("b2b"))
			{
				query="update catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id set cp.price=cp.price+1,cp.status=1,cp.custom_int_5=6,inv.updated_at=now() where cp.id="+catalog_b2b_pid;
				update=db.insertUpdateRecordWithConnectObj(con, query);
				
				query="update catalog_product cp join mktplace.inventory inv on cp.id=inv.product_id set cp.price=cp.price+1,cp.status=1,cp.custom_int_5=6,inv.updated_at=now() where cp.id="+catalog_mystore_pid;
				update=db.insertUpdateRecordWithConnectObj(con, query);
			}
		}
	}

	public void updateInfo(String productid,JSONObject productInfo,Connection con) throws Exception
	{
		String query="update catalog_product set info='"+productInfo+"' where id="+productid;
		int update=db.insertUpdateRecordWithConnectObj(con, query);
	}
	
	
	public LinkedHashMap<String, Object> expectedComplexCatalogESIndex(LinkedHashMap<String, Integer> productids,Xls_Reader indexResult)
	{
		LinkedHashMap<String, Object> expectedIndexed = new LinkedHashMap<String,Object>();
		for(int rowNum=2;rowNum<=indexResult.getRowCount(sheetName)-1;rowNum++)
		//for(int rowNum=7;rowNum<=7;rowNum++)
		{
			String testCase=indexResult.getCellData(sheetName, "Test Cases", rowNum);
			LinkedHashMap<String, String> indexValues = new LinkedHashMap<String, String>();
		
			String complex_pid=indexResult.getCellData(sheetName, "Complex Product ID", rowNum);
			String catalog_qc_pid=indexResult.getCellData(sheetName, "catalog_qc", rowNum);
			String catalog_v3_pid=indexResult.getCellData(sheetName, "catalog_v3", rowNum);
			String catalog_offline_pid=indexResult.getCellData(sheetName, "catalog_offline", rowNum);
			String catalog_utility_pid=indexResult.getCellData(sheetName, "catalog_utility", rowNum);
			String catalog_b2b_value=indexResult.getCellData(sheetName, "catalog_b2b_v1", rowNum);
			String catalog_mystore_value=indexResult.getCellData(sheetName, "catalog_mystore_v1", rowNum);
			
			String search_index_pid=indexResult.getCellData(sheetName, "search_v4", rowNum);
			String search_index_offline_pid=indexResult.getCellData(sheetName, "offline_products", rowNum);
			String search_index_b2b_value=indexResult.getCellData(sheetName, "search_b2b", rowNum);
			String search_index_mystore_value=indexResult.getCellData(sheetName, "search_mystore", rowNum);
			
			indexValues.put("catalog_qc",catalog_qc_pid);
			indexValues.put("catalog_v3",catalog_v3_pid);	
			indexValues.put("catalog_offline",catalog_offline_pid);
			indexValues.put("catalog_utility_v3",catalog_utility_pid);
			indexValues.put("catalog_b2b_v1",catalog_b2b_value);
			indexValues.put("catalog_mystore_v1",catalog_mystore_value);
			indexValues.put("search_v4",search_index_pid);
			indexValues.put("offline_products",search_index_offline_pid);
			indexValues.put("b2b_alias",search_index_b2b_value);
			indexValues.put("mystore_alias",search_index_mystore_value);
			
			if(testCase.toLowerCase().contains("dissociate"))
			{
				String search_variant=indexResult.getCellData(sheetName, "search_by_variant", rowNum);
				indexValues.put("search_by_variant",search_variant);		
			}
			
			expectedIndexed.put(testCase, indexValues);
		}
		return expectedIndexed;
	}
	
	@SuppressWarnings("unchecked")
	public static void validateData(LinkedHashMap<String, Object> actualIndexedData,LinkedHashMap<String, Object> expectedIndexedData,Xls_Reader indexResult)
	{
		String status="";
		for(int rowNum=2;rowNum<=indexResult.getRowCount(sheetName)-1;rowNum++)
		//for(int rowNum=7;rowNum<=7;rowNum++)
		{
			String testCase=indexResult.getCellData(sheetName, "Test Cases", rowNum);
			log = extent.startTest("TestCase::: "+testCase);
			System.out.println("***********Validating for test case :::: "+testCase+"****************");
			log.log(LogStatus.INFO,"Validate Data!!" );
			String complexpid=indexResult.getCellData(sheetName, "Complex Product ID", rowNum);
			log.log(LogStatus.INFO,"Complex Id:::" +complexpid);
			LinkedHashMap<String, String> actualProductindexesValues = new LinkedHashMap<String, String>();
			LinkedHashMap<String, String> expectedProductindexesValues = new LinkedHashMap<String, String>();
			LinkedHashMap<String, String> failedIndex = new LinkedHashMap<String, String>();
			if(actualIndexedData.containsKey(testCase) && expectedIndexedData.containsKey(testCase))
			{
				
				actualProductindexesValues=(LinkedHashMap<String,String>) actualIndexedData.get(testCase);
				expectedProductindexesValues=(LinkedHashMap<String, String>) expectedIndexedData.get(testCase);
				System.out.println("Actual::"+actualProductindexesValues);
				System.out.println("Expected:::"+expectedProductindexesValues);
				failedIndex.clear();
				ArrayList<String> tempResult=new ArrayList<>();
				for(String key:actualProductindexesValues.keySet())
				{
					String actual=actualProductindexesValues.get(key);
					String expected=expectedProductindexesValues.get(key);
					
					if(actual.contains("|") || expected.contains("|"))
					{
						List<String> myList = new ArrayList<String>(Arrays.asList(actual.replaceAll(" ","").split("\\|")));
						Collections.sort(myList);
						actual=myList.toString().trim().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "|").replaceAll(" ", "").trim();
						
						myList = new ArrayList<String>(Arrays.asList(expected.replaceAll(" ","").split("\\|")));
						Collections.sort(myList);
						expected=myList.toString().trim().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", "|").replaceAll(" ", "").trim();
					}
					
					if(actual.equalsIgnoreCase(expected))
						tempResult.add("Pass");
					else
					{
						tempResult.add("Fail");
						failedIndex.put(key,"Actual Data::: "+"->>"+actual+" :::: Expected Data:::"+expected);
					}
				}
				if(!tempResult.contains("Fail"))
					status="Pass";
				else
					status="Fail";
			}
			else
				status="Fail || Either actual map or expected map does not contain keys";
			
			if(!status.contains("Fail"))
			{
				log.log(LogStatus.PASS, "Product got indexed on expected indexes");
				log.log(LogStatus.INFO, "Actual Data::: "+actualProductindexesValues);
				log.log(LogStatus.INFO, "Expected Data::: "+expectedProductindexesValues);
			}
			else
			{
				log.log(LogStatus.FAIL, "Product does not got indexed on expected indexes");
				log.log(LogStatus.INFO, "Failed Indexes!!!!");
				log.log(LogStatus.INFO, failedIndex.toString());
				
			}
			
			indexResult.setCellData(sheetName, "Actual Result", rowNum, actualProductindexesValues.toString(), headerCount);
			indexResult.setCellData(sheetName, "Expected Result", rowNum, expectedProductindexesValues.toString(), headerCount);
			indexResult.setCellData(sheetName, "Result", rowNum, status, headerCount);
		}	
	}
	
	@AfterTest
	public void revertData() throws Exception
	{
		con = db.DBConnection(common_constants.serverName);
		String query="update catalog_product set status=1 where id="+disableStandaloneUtility;
		int update=db.insertUpdateRecordWithConnectObj(con, query);
		System.out.println("Updated data reverted ::: "+update);
		
		query="update catalog_product set visibility=3,parent_id=1200171854 where id="+dissociateOnlineProduct;
		update=db.insertUpdateRecordWithConnectObj(con, query);
		System.out.println("Updated data reverted ::: "+update);
		
		
	}
		
}
