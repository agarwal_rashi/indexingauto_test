package com.paytm.test.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.collect.Sets;
import com.paytm.reusable_methods.common_constants;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class esIndex 
{	
	
	String esUrl="";
	String searchindexUrl="";
	String searchOfflineIndexUrl="";
	String indexType="";
	RequestSpecification request=RestAssured.given();
	ArrayList<String> notIndexed =new ArrayList<String>(); 
	ArrayList<String> notIndexedProductId =new ArrayList<String>(); 
		
	public ArrayList<String> hitESIndex(Set<String> qcProducts,Set<String> digitalProducts,Set<String> onlineProducts,Set<String> offlineProducts)
	{
		
		LinkedHashMap<String,Set> products=new LinkedHashMap<String,Set>();
		Set<String> product=new HashSet<String>();
		
		
		products.put("qc",qcProducts);
		products.put("digital",digitalProducts);
		products.put("online",onlineProducts);
		products.put("offline",offlineProducts);
		
		for(String productSelection:products.keySet())
		{
			product=indexSelection(products,productSelection);
			notIndexed=checkIndex(product);
			notIndexedProductId.addAll(notIndexed);
		}
		
		System.out.println("Not Indexed Product Id's::: ");
		for(int index=0;index<notIndexedProductId.size();index++)
			System.out.println(notIndexedProductId.get(index));
		
		return notIndexedProductId;
		
	}
	
	public void hitESIndex(LinkedHashMap<String, Integer> productids)
	{
		LinkedHashMap<String,Set> products=new LinkedHashMap<String,Set>();
		Set<String> standaloneProducts = new HashSet<String>();
		
		Set<String> product=new HashSet<String>();
		
		for(String testCase:productids.keySet())
		{
			standaloneProducts.add(String.valueOf(productids.get(testCase)));
			if(testCase.contains("qc"))
				products.put("qc",standaloneProducts);
			else if(testCase.contains("digital"))
				products.put("digital",standaloneProducts);
			else if(testCase.contains("online"))
				products.put("online",standaloneProducts);
			else if(testCase.contains("offline"))
				products.put("offline",standaloneProducts);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Set<String> indexSelection(LinkedHashMap<String, Set> products,String productKey)
	{
		Set<String> productType=new HashSet<String>();
		productType=products.get(productKey);
		switch(productKey)
		{
			case "qc":
				indexType="catalog_qc";
				
				return productType;
			case "digital":
				indexType="catalog_v3_utility";
				return productType;
			case "online":
				indexType="catalog_v3";
				return productType;
			case "offline":
				indexType="catalog_offline";
				return productType;
		}
		return productType;

	}
	
	public HashMap<String,JSONObject>  checkCategoryIndex(Set<String> categoryType)
	{
		JSONObject parentSourceArray=null;
		JSONArray variantsArray=null;
		HashMap<String,JSONObject> catProduct=new LinkedHashMap<String,JSONObject>();
		for(String category:categoryType)
		{
			esUrl=common_constants.searchesURL;//To run on sanity
			if(!common_constants.defineProductSize.isEmpty())
				esUrl=esUrl+"refiner/_search?q=category_id:"+category+"&size="+common_constants.defineProductSize;
			else
				esUrl=esUrl+"refiner/_search?q=category_id:"+category;
			//esUrl=common_constants.liveSearchesURL;//To run on Prod
			//esUrl=esUrl+"_search?cats:"+category+"&size=1";
			System.out.println("ES URL:::"+esUrl);
	
			Response response=request.get(esUrl);
			ResponseBody message =response.getBody();
			System.out.println("Message returned: "+message.asString());
			
			try
			{
				JSONObject messageResponse=new JSONObject(message.asString());
				JSONArray parentProductArray=messageResponse.getJSONObject("hits").getJSONArray("hits");
				System.out.println("Parent product::"+parentProductArray);
				String parentProductid="";
				for(Object object:parentProductArray)
				{
					JSONObject hit=(JSONObject) object;
					parentSourceArray=hit.getJSONObject("_source");
					parentProductid=hit.getString("_id");
					//hit.keys().forEachRemaining(action);;
					variantsArray=parentSourceArray.getJSONArray("variants");
					parentSourceArray.put("Complex_id", parentProductid);
					parentSourceArray.put("Variants", variantsArray);
					
					//Key formation will be category_id_product_id
					catProduct.put(category+"_"+parentProductid, parentSourceArray);
				}
				
			}
			catch(Exception e)
			{
				System.out.println("Exception Caught for category id::"+category);
				e.printStackTrace();
			}
			
		}
		return catProduct;
	}
	
	public  HashMap<String,JSONObject> hitCatalogUtilityIndex(Set<String> digitalProductsResult,String indexType)
	{
		JSONObject parentSourceArray=null;
		JSONArray variantsArray=null;
		String parentProductid="";
		HashMap<String,JSONObject> products=new LinkedHashMap<String,JSONObject>();
		
		esUrl=common_constants.esURL+indexType+"/_search?size=100";
		String apiBody="{\"query\":{\"bool\":{\"must\":[{\"terms\":{\"id\":"+digitalProductsResult+"}}]}}}";
		request.body(apiBody);
		
		System.out.println("API Body::: "+apiBody);
		Response response=request.get(esUrl);
		ResponseBody message =response.getBody();
		System.out.println("Message returned: "+message.asString());
		try
		{
			JSONObject messageResponse=new JSONObject(message.asString());
			JSONArray parentProductArray=messageResponse.getJSONObject("hits").getJSONArray("hits");
			System.out.println("Parent product::"+parentProductArray);
			for(Object object:parentProductArray)
			{
				JSONObject hit=(JSONObject) object;
				parentSourceArray=hit.getJSONObject("_source");
				parentProductid=hit.getString("_id");
				variantsArray=parentSourceArray.getJSONArray("variants");
				parentSourceArray.put("Variants", variantsArray);
				//Long childProductid=variantsArray.getLong("id");
				products.put(parentProductid, parentSourceArray);
			}
			
		}
		catch(Exception e)
		{
			System.out.println("Exception Caught for product id::"+parentProductid);
			e.printStackTrace();
		}
		return products;
	}
	
	/*public LinkedHashMap<String, Object> hitComplexCatalogESIndex(LinkedHashMap<String, Integer> productids,String[] indexTypes,String dissociateProrductId)
	{
		searchOfflineIndexUrl=common_constants.searchOfflineIndexURL;
		LinkedHashMap<String, Object> indexed = new LinkedHashMap<String,Object>();
		for(String testCase:productids.keySet())
		{
			System.out.println("Running for test Case :::: "+testCase);
			System.out.println("Complex ID::: "+productids.get(testCase));
			LinkedHashMap<String,String> variantsIndexed = new LinkedHashMap<String, String>();
			//ArrayList<Object> variantsIndexed=new ArrayList<Object>();
			LinkedHashMap<String,String> variants = new LinkedHashMap<String, String>();
			int productid=productids.get(testCase);
			for(String indexType:indexTypes)
			{
				esUrl=common_constants.esURL;
				esUrl=esUrl+indexType+"/refiner/"+productid;
				Response responseCatalog=request.get(esUrl);
				ResponseBody catalogMessage =responseCatalog.getBody();
				//System.out.println("Message returned by catalog ES Index: "+catalogMessage.asString());
	
				variants=getVariantId(catalogMessage,indexType,"complex");
				//variantsIndexed.add(variants);
				variantsIndexed.put(indexType, variants.get(indexType));
				
			}
			//To check on search ES
			searchindexUrl=common_constants.searchIndexURL;
			searchindexUrl=searchindexUrl+productid;
			Response responseSearch=request.get(searchindexUrl);
			ResponseBody searchMessage =responseSearch.getBody();
			//System.out.println("Message returned by search ES Index: "+searchMessage.asString());
			variants=getVariantId(searchMessage,"search_v4","complex");
			variantsIndexed.put("search_v4", variants.get("search_v4"));
			
			//To check on search offline
			searchOfflineIndexUrl=searchOfflineIndexUrl+productid;
			Response responseSearchOffline=request.get(searchOfflineIndexUrl);
			ResponseBody searchOfflineMessage =responseSearchOffline.getBody();
			//System.out.println("Message returned by search offline ES Index: "+searchOfflineMessage.asString()); 
			variants=getVariantId(searchOfflineMessage,"offline_products","complex");
			variantsIndexed.put("offline_products", variants.get("offline_products"));
			
			if(testCase.toLowerCase().contains("dissociate"))
			{
				esUrl=common_constants.esURL;
				esUrl=esUrl+"catalog_v3/refiner/"+dissociateProrductId;
				Response responseCatalog=request.get(esUrl);
				ResponseBody catalogMessage =responseCatalog.getBody();
				variants=getVariantId(catalogMessage,indexType,"complex");
				variantsIndexed.put("search_by_variant", variants.get(indexType));
			}
			
			indexed.put(testCase, variantsIndexed);
		}
		return indexed;
	}*/
	
	public LinkedHashMap<String, Object> hitComplexCatalogESIndex(LinkedHashMap<String, Integer> productids,String[] indexTypes,String[] searchindexTypes,String dissociateProrductId)
	{
		searchOfflineIndexUrl=common_constants.searchOfflineIndexURL;
		LinkedHashMap<String, Object> indexed = new LinkedHashMap<String,Object>();
		for(String testCase:productids.keySet())
		{
			System.out.println("Running for test Case :::: "+testCase);
			System.out.println("Complex ID::: "+productids.get(testCase));
			LinkedHashMap<String,String> variantsIndexed = new LinkedHashMap<String, String>();
			//ArrayList<Object> variantsIndexed=new ArrayList<Object>();
			LinkedHashMap<String,String> variants = new LinkedHashMap<String, String>();
			int productid=productids.get(testCase);
			for(String indexType:indexTypes)
			{
				esUrl=common_constants.esURL;
				esUrl=esUrl+indexType+"/refiner/"+productid;
				Response responseCatalog=request.get(esUrl);
				ResponseBody catalogMessage =responseCatalog.getBody();
				//System.out.println("Message returned by catalog ES Index: "+catalogMessage.asString());
	
				variants=getVariantId(catalogMessage,indexType,"complex");
				//variantsIndexed.add(variants);
				variantsIndexed.put(indexType, variants.get(indexType));
				
			}
			//To check on search ES
			
			for(String searchIndex:searchindexTypes)
			{
				searchindexUrl=common_constants.searchURL;
				
				searchindexUrl=searchindexUrl+searchIndex+"/refiner/"+productid;
				Response responseCatalog=request.get(searchindexUrl);
				ResponseBody catalogMessage =responseCatalog.getBody();
				variants=getVariantId(catalogMessage,searchIndex,"complex");
				variantsIndexed.put(searchIndex, variants.get(searchIndex));
			}
			if(testCase.toLowerCase().contains("dissociate"))
			{
				esUrl=common_constants.esURL;
				esUrl=esUrl+"catalog_v3/refiner/"+dissociateProrductId;
				Response responseCatalog=request.get(esUrl);
				ResponseBody catalogMessage =responseCatalog.getBody();
				variants=getVariantId(catalogMessage,indexType,"complex");
				variantsIndexed.put("search_by_variant", variants.get(indexType));
			}
			
			indexed.put(testCase, variantsIndexed);
		}
		return indexed;
	}
	
	
	public LinkedHashMap<String, String> getVariantId(ResponseBody message,String indexType,String methodType)
	{
		LinkedHashMap<String,String> variants = new LinkedHashMap<String, String>();
		try 
		{
			JSONObject complexResponse=new JSONObject(message.asString());
			boolean catalogValue=complexResponse.getBoolean("found");
			if(catalogValue&&methodType.equalsIgnoreCase("complex"))
			{
				JSONObject sourceObject=complexResponse.getJSONObject("_source");
				JSONArray variantsArray=sourceObject.getJSONArray("variants");
				for(Object object:variantsArray)
				{
					JSONObject variantObject=(JSONObject) object;
					Long variantId= variantObject.getLong("id");
					if(variants.containsKey(indexType))
						variants.put(indexType,variants.get(indexType).toString()+"|"+variantId.toString());
					else
						variants.put(indexType, variantId.toString());
				}
			}
			else if(methodType.equalsIgnoreCase("standalone")||(methodType.equalsIgnoreCase("complex")&&catalogValue==false))
			{
				String variantId=String.valueOf(catalogValue);
				variants.put(indexType, variantId.toUpperCase());
			}
				
		} 
		catch (Exception e) 
		{
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
		return variants;
	}
	
	@SuppressWarnings("rawtypes")
	public LinkedHashMap<String, Object>  hitStandaloneCatalogESIndex(LinkedHashMap<String, Integer> productids,int associateComplexProductId)
	{
		LinkedHashMap<String, Object> indexed = new LinkedHashMap<String,Object>();
		String indexTypes[]={"catalog_qc","catalog_v3","catalog_utility_v3","catalog_offline","catalog_b2b_v1","catalog_mystore_v1"};
		String searchindexTypes[]={"search_v4","offline_products","b2b_alias","mystore_alias"};
		
		for(String testCase:productids.keySet())
		{
			LinkedHashMap<String, Boolean> productsIndexed = new LinkedHashMap<String, Boolean>();
			LinkedHashMap<String, String>  variants= new LinkedHashMap<String, String>();
			int productid=productids.get(testCase);
			System.out.println("Hitting ES for "+testCase+"::: "+productid);
			for(String indexType:indexTypes)
			{
				esUrl=common_constants.esURL;
				//searchindexUrl=common_constants.searchIndexURL;
				
				esUrl=esUrl+indexType+"/refiner/"+productid;
				Response responseCatalog=request.get(esUrl);
				ResponseBody catalogMessage =responseCatalog.getBody();
				variants=getVariantId(catalogMessage,indexType,"standalone");
				productsIndexed.put(String.valueOf(productid)+"_"+indexType, Boolean.valueOf(variants.get(indexType)));
			}
			
			for(String searchIndex:searchindexTypes)
			{
				searchindexUrl=common_constants.searchURL;
				
				searchindexUrl=searchindexUrl+searchIndex+"/refiner/"+productid;
				Response responseCatalog=request.get(searchindexUrl);
				ResponseBody catalogMessage =responseCatalog.getBody();
				variants=getVariantId(catalogMessage,searchIndex,"standalone");
				productsIndexed.put(String.valueOf(productid)+"_"+searchIndex, Boolean.valueOf(variants.get(searchIndex)));
			}
			/*searchindexUrl=searchindexUrl+productid;
			Response responseSearch=request.get(searchindexUrl);
			ResponseBody searchMessage =responseSearch.getBody();
			variants=getVariantId(searchMessage,"search_v4","standalone");
			productsIndexed.put(String.valueOf(productid)+"_search", Boolean.valueOf(variants.get(indexType)));*/
			if(testCase.contains("Associate"))
			{
				ArrayList<Integer> variantProducts=new ArrayList<Integer>();
				esUrl=common_constants.esURL;
				esUrl=esUrl+"catalog_v3"+"/refiner/"+associateComplexProductId;
				System.out.println("Hitting ES for "+testCase+"::: "+associateComplexProductId);
				System.out.println("ES URL::: "+esUrl);
				Response responseCatalog=request.get(esUrl);
				ResponseBody catalogMessage =responseCatalog.getBody();
				try 
				{
					JSONObject complexResponse=new JSONObject(catalogMessage.asString());
					JSONObject sourceObject=complexResponse.getJSONObject("_source");
					JSONArray variantsArray=sourceObject.getJSONArray("variants");
					for(Object object:variantsArray)
					{
						JSONObject variantObject=(JSONObject) object;
						//Long id=hit.getJSONObject("_source").getLong("id");
						Long variantId= variantObject.getLong("id");
						variantProducts.add(Integer.valueOf(variantId.toString()));
					}
					int standaloneProductid=productids.get(testCase);
					productsIndexed.put(String.valueOf(associateComplexProductId)+"_search_by_complex", false);
					for(int variantid:variantProducts)
					{
						if(variantid==standaloneProductid)
						{
							System.out.println("Variant found in complex:::"+variantid);
							productsIndexed.put(String.valueOf(associateComplexProductId)+"_search_by_complex", true);
						}
						
					}	
				} 
				catch (Exception e) 
				{
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
			}
			
			/*else if(testCase.contains("Offline") || testCase.contains("Change discoverability"))
			{
				searchOfflineIndexUrl=common_constants.searchOfflineIndexURL;
				searchOfflineIndexUrl=searchOfflineIndexUrl+productid;
				Response responseSearchOffline=request.get(searchOfflineIndexUrl);
				ResponseBody searchOfflineMessage =responseSearchOffline.getBody();
				variants=getVariantId(searchOfflineMessage,"offline_products","standalone");
				productsIndexed.put(String.valueOf(productid)+"_search_offline", Boolean.valueOf(variants.get(indexType)));
			}*/
			
			indexed.put(testCase, productsIndexed);
		}
		
		return indexed;
	}
	
	public ArrayList<String> checkIndex(Set<String> productType)
	{
		ArrayList<String> notIndexedProductId =new ArrayList<String>();
		Set<String> indexedProductId =new HashSet<String>();
		Set<String> notIndexed =new HashSet<String>();
		//for(String product:productType)
		//{
			esUrl=common_constants.esURL;
			//esUrl=esUrl+indexType+"/_search/"+product;
			esUrl=esUrl+indexType+"/_search";
			String apiBody="{\"query\":{\"bool\":{\"must\":[{\"terms\":{\"id\":"+productType+"}}]}}}";
			System.out.println("*****ES URL********");
			
			System.out.println(esUrl);
			System.out.println("*****API BODY********");
			System.out.println(apiBody);
			request.body(apiBody);
			Response response=request.get(esUrl);
			ResponseBody message =response.getBody();
			System.out.println("Message returned: "+message.asString());
			try 
			{
				JSONObject jsonresponse=new JSONObject(message.asString());
				int value=jsonresponse.getJSONObject("hits").getInt("total");
				
				System.out.println("Product Id's found on ES ::: "+value);
				
				if(value>0)
				{
					JSONArray hitsArray=jsonresponse.getJSONObject("hits").getJSONArray("hits");
					//System.out.println(hitsArray);
					
					for(Object object:hitsArray)
					{
						JSONObject hit=(JSONObject) object;
						Long id=hit.getJSONObject("_source").getLong("id");
						//String id=hit.getString("_id");
						System.out.println("Product id's found on URL:::"+esUrl+id);
						indexedProductId.add(String.valueOf(id));
					}
				}
				else
				{
					notIndexedProductId=new ArrayList<>(productType);
				}
				
				/*for(String productidIndexed:indexedProductId)
					System.out.println("Product Ids Found::: "+productidIndexed);*/
				
				if(!indexedProductId.isEmpty())
				{
					notIndexed=Sets.difference(productType,indexedProductId);
					notIndexedProductId=new ArrayList<>(notIndexed);
				}
				
				/*for(int index=0;index<notIndexedProductId.size();index++)
					System.out.println("Product Ids Found::: "+notIndexedProductId.get(index));*/
					
				
			} 
			catch (Exception e) 
			{
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
			

			return notIndexedProductId;
			
			/*try 
			{
				ObjectMapper mapper = new ObjectMapper();
			
				JsonNode actualObj =  mapper.readTree(message.asString());
				Iterator<Entry<String, JsonNode>> test=actualObj.fields();
				while (test.hasNext()) {
				    Map.Entry<String, JsonNode> pair = test.next();
				    if(pair.getKey().equalsIgnoreCase("found"))
				    {
				    	String value=pair.getValue().toString();
				    	//System.out.println(pair.getKey() + pair.getValue());
				    	if(value.equalsIgnoreCase("false"))
				    		notIndexedProductId.add(product);
				    	break;
				    		
					 }
				}
			} 
			catch (IOException e) 
			{
			// TODO Auto-generated catch block
			e.printStackTrace();
			}*/
		
		
	}
	

}
