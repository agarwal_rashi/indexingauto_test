package com.paytm.reusable_methods;

import java.text.SimpleDateFormat;
import java.util.Date;

public class common_constants
{
	private static String classPathtemp = "";
	static
	{
		classPathtemp = System.getProperty("user.dir") + "/src";
	}
	public static String browser = "chrome";
	
	public static final String siteURL = "https://seller-dev.paytm.com"; 
	public static final String esURL= "http://10.254.41.12:9200/";
	public static final String searchURL="http://10.254.26.210:9200/";
	
	public static final String searchesURL= "http://10.254.26.210:9200/search_v4/";
	public static final String searchIndexURL="http://10.254.26.210:9200/search_v4/refiner/";
	
	public static final String searchOfflineIndexURL="http://10.254.26.210:9200/offline_products/refiner/";
	public static  final String liveSearchesURL="http://internal-marketplace-searches-intv2-479202002.ap-southeast-1.elb.amazonaws.com:8080/search_filter/";
	public static  final String defineProductSize="2";

	
	//login credentials
	public static final String seller_username="rashi.agarwal@paytmmall.com";
	public static final String seller_password="paytm@123";
	
	public static final String classPath = classPathtemp;
	public static final String dataFolderName = "Test Data";
	public static final String serverName = "staging";
	public static final String merchantID="600721";
	
	public static final int onlineDiscoverability=1;
	public static final int offlineDiscoverability=2;
	public static final int b2bDiscoverability=3;
	public static final int mystoreDiscoverability=4;
	
	
	static Date date = new Date();
	static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy_hh-mm-ss_a");
	public static String currentTimeStamp = sdf.format(date);
	
}
