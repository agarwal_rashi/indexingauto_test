package com.paytm.reusable_methods;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.tools.ant.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


public class generic_methods 
{
	public boolean login_seller(WebDriver driver)
	{
		boolean is_login=false;
		try
		{
			By username=By.cssSelector("#username");
			By password= By.cssSelector("#password");
			By login=By.xpath("//a[contains(@ng-click,'logIn')]");
			
			driver.findElement(username).clear();
			driver.findElement(password).clear();
			
			driver.findElement(username).sendKeys(common_constants.seller_username);
			driver.findElement(password).sendKeys(common_constants.seller_password);
			
			driver.findElement(login).click();
			is_login=true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return is_login;
	}
	
	public void logout_seller(WebDriver driver) throws InterruptedException
	{
		Actions act=new Actions(driver);
		WebElement profileMenu=driver.findElement(By.xpath("//a[@data-activates='headerdropdown']"));
		act.moveToElement(profileMenu);
		act.click().build().perform();
		Thread.sleep(300);
		By signOut=By.cssSelector("#headerdropdown li:last-child a");
		driver.findElement(signOut).click();	
	}
	
	public String getExceptionInfo(Exception e)	
	{
		String exceptionInfo = null;
		Integer lineNo = null;
		String methodName = null;
		String className = null;
		String exceptionReason = null;
		StackTraceElement ste = null;

		StackTraceElement[] stackInfo = e.getStackTrace();

		for (int i = 0; i < stackInfo.length; i++) 
		{
			ste = stackInfo[i];

			if(ste != null)
			{
				if(ste.toString().trim().toUpperCase().contains("src".toUpperCase()) )
				{
					lineNo = ste.getLineNumber();
					methodName =ste.getMethodName();
					className = ste.getClassName();
					exceptionReason = e.getClass().toString();

					//if(exceptionReason != null && className != null && methodName != null && lineNo != null)
					exceptionInfo = "Exception Message : "+e.getMessage()+" ["+exceptionReason+"] at Line No. '"+lineNo+"' in Method '"+methodName+"' of Class '"+className+"'";
					break;
				}
			}	 
		}

		return exceptionInfo;
	}
	
	public  String generateRandomString(int length)
	{
		String random="";
		random=RandomStringUtils.randomAlphabetic(length).toLowerCase();
		return random;
	}
	
	public int generateRandomNumber(int min,int max)
	{
		int number;
		//number=(int) (Math.random()*(max-min)+1);
		number=(int) Math.floor(Math.random() * ((max-min)+1) + min);
		return number;
		
	}
	
	public  File getLatestFilefromDir(String dirPath)
	{
	    File dir = new File(dirPath);
	    File[] files = dir.listFiles();
	    if (files == null || files.length == 0) 
	        return null;
	
	    File lastModifiedFile = files[0];
	    for (int i = 1; i < files.length; i++) 
	    {
	       if (lastModifiedFile.lastModified() < files[i].lastModified())
	           lastModifiedFile = files[i];
	    }
	    return lastModifiedFile;
	}
	
	public  String renameExistingFile(File fileDetails,String directoryPath)
	{
		System.out.println("Directory path::"+directoryPath);
		 File backupFile=new File(directoryPath+"//"+common_constants.currentTimeStamp+"_"+fileDetails.getName());
		 String error_file_name="";
		 fileDetails.renameTo(backupFile);
		 fileDetails= getLatestFilefromDir(directoryPath);
		 error_file_name=fileDetails.getName();
		 return error_file_name;
	}
	
	public void copyFileUsingApacheCommonsIO(File source, File dest) throws IOException {
	    FileUtils.copyFile(source, dest);
	}
	
	public JSONObject setDiscoverabilityInfo(String productId,List<Integer> passList,Connection con) throws Exception
	{
		DBFunctions db = new DBFunctions();
		String query="select info from catalog_product where id="+productId;
		//String result=db.SelectQuery(1,query, common_constants.serverName);
		ArrayList<String> result=new ArrayList<String>();
		result=db.getParticularRecordsUsingExistingConnection(query,con);
		String info=result.toString().replaceFirst("\\[", "");
		StringBuilder builder=new StringBuilder(info);
		info=builder.deleteCharAt(info.length()-1).toString();
		JSONObject productInfo = null;
		if(!info.equalsIgnoreCase("null"))
		{
			try
			{
				productInfo=new JSONObject(info);
				JSONArray discoverabilityArray=productInfo.getJSONArray("discoverability");
				List<Integer> fetchValue=new ArrayList<Integer>();
				for(Object value:discoverabilityArray)
				{
					fetchValue.add((int)value);
				}
				boolean flag=fetchValue.equals(passList);
				if(!flag)
				{
					productInfo.remove("discoverability");
					productInfo.put("discoverability", passList);
				}
				
			}
			catch(Exception e)
			{
				System.out.println("Exception Caught for product id::"+productId);
				e.printStackTrace();
			}
		}
		
		return productInfo;
	}
	
	public String capture_screenshot(WebDriver driver,String screenShotName)
	{
		try
		{
			TakesScreenshot ts= (TakesScreenshot)driver;
			File source=ts.getScreenshotAs(OutputType.FILE);
			String screenshot_destination=common_constants.classPath + "/main/java/com/paytm/com/html_reports/Screenshots/AddVertical_" +screenShotName+"_"+ common_constants.currentTimeStamp+".png";
			File destination=new File(screenshot_destination);
			FileUtils.copyFile(source, destination);
			System.out.println("Screenshot Taken");
			return screenshot_destination;
		}
		catch(Exception e)
		{
			System.out.println("Exception while taking screenshot::"+e.getMessage());
			return e.getMessage();	
		}
	}
	
	public  boolean isValidURL(String url)
	{
		try 
		{
            new URL(url).toURI();
            return true;
        }
        catch (Exception e)
		{
            return false;
        }
	}
	
	public  Xls_Reader removeHeaders(Xls_Reader bulkProductUpload,String sheet,int startingColumn,int endingColumn)
	{
		for(int i=startingColumn;i<=endingColumn;i++)
			bulkProductUpload.removeColumn(sheet,i);	
		
		return bulkProductUpload;
	}
	//Functions used in API's:Update Product
	public FileWriter writeCSVFile(String fileName,int productid,LinkedHashMap<String, String> headerWithValues)
	{
		final String COMMA_DELIMITER = ",";
		final String NEW_LINE_SEPARATOR = "\n";
		
		FileWriter fileWriter=null;
		try
		{
			fileWriter=new FileWriter(fileName);
			fileWriter.append("Product Id").append(COMMA_DELIMITER);
			//Set Headers Values in CSV
			for(String header:headerWithValues.keySet())
				fileWriter.append(header).append(COMMA_DELIMITER);
			fileWriter.append(NEW_LINE_SEPARATOR);
			fileWriter.append(String.valueOf(productid)).append(COMMA_DELIMITER);
			//Set Values in CSV
			for(String header:headerWithValues.keySet())
				fileWriter.append(headerWithValues.get(header)).append(COMMA_DELIMITER);
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			System.out.println("CSV file was created successfully !!!");
		}
		catch (Exception e)
		{
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}
		finally 
		{
			try
			{
		      fileWriter.flush();
		      fileWriter.close();
			}
			catch (IOException e) 
			{
		        System.out.println("Error while flushing/closing fileWriter !!!");
		        e.printStackTrace();
		    }
		}
		
		return fileWriter;

	}
	
	//Function Called when single column has to written in CSV
		public FileWriter writeCSVFile(String fileName,ArrayList<String> headers,ArrayList<String> values)
		{
			final String COMMA_DELIMITER = ",";
			final String NEW_LINE_SEPARATOR = "\n";
			
			FileWriter fileWriter=null;
			try
			{
				fileWriter=new FileWriter(fileName);
				//fileWriter.append("Product Id").append(COMMA_DELIMITER);
				
				//Set Headers Values in CSV
				for(int header=0;header<headers.size();header++)
				{
					fileWriter.append(headers.get(header));
					if(header<headers.size()-1)
						fileWriter.append(COMMA_DELIMITER);
				}
						
				fileWriter.append(NEW_LINE_SEPARATOR);
				
				//Set Values in CSV
				for(int index=0;index<values.size();index++)
					fileWriter.append(values.get(index)).append(NEW_LINE_SEPARATOR);
				
				System.out.println("CSV file was created successfully !!!");
			}
			catch (Exception e)
			{
				System.out.println("Error in CsvFileWriter !!!");
				e.printStackTrace();
			}
			finally 
			{
				try
				{
			      fileWriter.flush();
			      fileWriter.close();
				}
				catch (IOException e) 
				{
			        System.out.println("Error while flushing/closing fileWriter !!!");
			        e.printStackTrace();
			    }
			}
			
			return fileWriter;

		}
	
	
	
	//Function Called in dissociateProductCSVAPI
	public FileWriter writeCSVFile(String fileName,HashMap<String, String> fetchProducts,ArrayList<String> headers,ArrayList<String> values)
	{
		final String COMMA_DELIMITER = ",";
		final String NEW_LINE_SEPARATOR = "\n";
		
		FileWriter fileWriter=null;
		try
		{
			fileWriter=new FileWriter(fileName);
			fileWriter.append("Product Id").append(COMMA_DELIMITER);
			
			//Set Headers Values in CSV
			for(int header=0;header<headers.size();header++)
			{
				fileWriter.append(headers.get(header));
				if(header<headers.size()-1)
					fileWriter.append(COMMA_DELIMITER);
			}
					
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			//Set Values in CSV
			for(String productType:fetchProducts.keySet() )
				fileWriter.append(String.valueOf(fetchProducts.get(productType))).append(COMMA_DELIMITER).append(values.get(0)).append(NEW_LINE_SEPARATOR);
			
			System.out.println("CSV file was created successfully !!!");
		}
		catch (Exception e)
		{
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}
		finally 
		{
			try
			{
		      fileWriter.flush();
		      fileWriter.close();
			}
			catch (IOException e) 
			{
		        System.out.println("Error while flushing/closing fileWriter !!!");
		        e.printStackTrace();
		    }
		}
		
		return fileWriter;

	}

}
